# Web

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

The project is located in the folder "/web"

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Styles

You can change the colors of the app in '/web/styles/variables.scss'.

## Environments

You can change the name of the site and the api in '/web/environments/*'.


