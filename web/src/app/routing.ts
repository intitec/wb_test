import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {HomeComponent} from '../pages/home/component';
import {ListComponent} from '../pages/list/component';

const appRoutes: Routes = [
  { path: 'list/:category/:q', component: ListComponent },
  { path: 'list/:category', component: ListComponent },
  { path: '', component: HomeComponent },
  { path: '**', component: HomeComponent }
];

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
