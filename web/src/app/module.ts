/* ==== Angular.
 ============================= */

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

/* ==== Roting.
 ============================= */

import {routing, appRoutingProviders}  from './routing';

/* ==== Pages.
 ============================= */

import {AppComponent} from './component';
import {HomeComponent} from '../pages/home/component';
import {ListComponent} from '../pages/list/component';

/* ==== Modals.
 ============================= */

import {DetailImageComponent, DetailImageService, PortalImageDirective} from '../modals/detail-image';

/* ==== Providers.
 ============================= */

import {ImagesProvider} from '../providers/images';

/* ==== NgModule.
 ============================= */

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ListComponent,
    DetailImageComponent,
    PortalImageDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    routing,
    appRoutingProviders
  ],
  entryComponents: [
    DetailImageComponent
  ],
  providers: [
    ImagesProvider,
    DetailImageService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule{

}
