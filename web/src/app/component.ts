import {Component, ViewChild} from '@angular/core';
import {PortalImageDirective, DetailImageService} from '../modals/detail-image';
import {environment} from "../environments/environment";

@Component({
  selector: 'app-root',
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class AppComponent{
  @ViewChild(PortalImageDirective) detailImagePortal:PortalImageDirective;
  name:string = environment.name;

  constructor(
    private detailImageSvc:DetailImageService
  ){

  }

  ngAfterViewInit(){
    this.detailImageSvc.pushEl(this.detailImagePortal);
  }
}
