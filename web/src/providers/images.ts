import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../environments/environment";

@Injectable()
export class ImagesProvider{
  public hitsPerPage:number = 20;
  public categories = [
    'fashion', 'nature', 'backgrounds', 'science', 'education', 'people', 'feelings', 'religion', 'health', 'places',
    'animals', 'industry', 'food', 'computer', 'sports', 'transportation', 'travel', 'buildings', 'business', 'music'
  ];

  constructor(
    protected httpClient: HttpClient
  ){

  }

  list(q:string, category:string = 'all', page:number = 1){
    let options:any = {params: {}};

    options.params.key = environment.apiKey;
    options.params.page = page;
    if(q) options.params.q = encodeURIComponent(q).replace(/%20/g, "+");
    if(category != 'all') options.params.category = category;

    return this.httpClient.get(environment.api, options);
  }
}
