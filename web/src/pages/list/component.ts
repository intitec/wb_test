import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ImagesProvider} from "../../providers/images";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DetailImageService, DetailImageComponent} from '../../modals/detail-image';
import {environment} from "../../environments/environment";
import {Title} from "@angular/platform-browser";

@Component({
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class ListComponent{
  searchForm:FormGroup;
  list:any = null;
  page:number = 1;
  private sub;

  constructor(
    private imagesPvd:ImagesProvider,
    private route:ActivatedRoute,
    private fb:FormBuilder,
    private imageDetailSvc:DetailImageService,
    private titleService:Title
  ){

  }

  ngOnInit(){
    this.titleService.setTitle(`Search - ${environment.name}`);

    this.searchForm = this.fb.group({
      q: ['', Validators.compose([Validators.required, Validators.minLength(100)])],
      category: ['all']
    });

    this.sub = this.route.params.subscribe((params:any) => {
      this.searchForm.controls['category'].setValue(params.category || '');
      this.searchForm.controls['q'].setValue(params.q || '');
    });

    this.search();
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }

  private search(){
    this.imagesPvd.list(
      this.searchForm.controls['q'].value,
      this.searchForm.controls['category'].value,
      this.page
    )
    .subscribe(
      data => this.list = data,
      error => console.error(error)
    );

    return false;
  }

  onSearch(e){
    !e || e.stopPropagation();

    this.page = 1;
    this.search();
  }

  onPrevious(e){
    !e || e.stopPropagation();
    this.page--;
    this.search();
  }

  onNext(e){
    !e || e.stopPropagation();

    this.page++;
    this.search();
  }

  openDetail(image){
    const el = this.imageDetailSvc.getEl();
    el.createDialog(DetailImageComponent, image);
  }
}
