import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from '@angular/router';
import {ImagesProvider} from '../../providers/images';
import {environment} from "../../environments/environment";
import {Title} from "@angular/platform-browser";

@Component({
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class HomeComponent{
  searchForm:FormGroup;

  constructor(
    private imagesPvd:ImagesProvider,
    private fb:FormBuilder,
    private router:Router,
    private titleService:Title
  ){

  }

  ngOnInit(){
    this.titleService.setTitle(`Home - ${environment.name}`);

    this.searchForm = this.fb.group({
      q: ['', Validators.compose([Validators.required, Validators.minLength(100)])],
      category: ['all']
    });
  }

  onSearch(e){
    !e || e.stopPropagation();

    this.router.navigate([`/list/${this.searchForm.controls['category'].value}/${this.searchForm.controls['q'].value}`]);
  }
}
