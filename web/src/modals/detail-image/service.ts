import {Injectable} from '@angular/core';
import {PortalImageDirective} from './directive';

@Injectable()
export class DetailImageService{
  private portal:PortalImageDirective;

  constructor(){

  }

  pushEl(portal:PortalImageDirective):void{
    this.portal = portal;
  }

  getEl():PortalImageDirective{
    return this.portal;
  }
}
