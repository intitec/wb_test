import {Directive, ComponentFactoryResolver, ComponentRef} from '@angular/core';
import {ViewContainerRef} from '@angular/core';
import {DetailImageComponent} from './component';

@Directive({
  selector: '[portal="detail-image"]'
})
export class PortalImageDirective{

  constructor(
      private viewContainer:ViewContainerRef,
      private componentFactoryResolver:ComponentFactoryResolver
  ){

  }

  createDialog(dialogComponent:{ new(): DetailImageComponent }, data:any):ComponentRef<DetailImageComponent>{
    this.viewContainer.clear();

    let dialogComponentFactory = this.componentFactoryResolver.resolveComponentFactory(dialogComponent);
    let dialogComponentRef = this.viewContainer.createComponent(dialogComponentFactory, 0);

    window.document.body.style.overflow = 'hidden';

    dialogComponentRef.instance.data = data;

    dialogComponentRef.instance.close.subscribe(() => {
      window.document.body.style.overflow = 'auto';
      dialogComponentRef.destroy();
    });

    return dialogComponentRef;
  }
}
