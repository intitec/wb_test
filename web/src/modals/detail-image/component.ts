import {Component, EventEmitter, Input} from '@angular/core';

@Component({
  selector: 'detail-image',
  templateUrl: './view.html',
  styleUrls: ['./style.scss']
})
export class DetailImageComponent{
  @Input() data:any;
  close = new EventEmitter();

  constructor(){

  }

  ngOnInit(){
    console.log(this.data);
  }

  onClickedExit(){
    this.close.emit();
  }

  onDownload(e){
    !e || e.stopPropagation();

    window.open(this.data.largeImageURL, '_blank');
  }
}
